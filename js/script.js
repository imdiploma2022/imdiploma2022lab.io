init();

// $(".line").click(darkMode);

function toggleNav() {
	let nav = $(".nav");
	let open = $(".open-nav");
	let display = nav.css("display");
	if (display == "block") {
		nav.hide();
		open.show();
	} else {
		nav.show();
		open.hide();
	}
}

function init() {
	// let dm = getLs("darkmode");
	// dm == "dark" ? setMode("dark") : setMode("light");
	let lang_switch = $(".lang");
	let lang = getLs("language");
	if (lang == "en") {
		$(".en").show();
		$(".hu").hide();
		lang_switch.html("HU");
	} else {
		$(".hu").show();
		$(".en").hide();
		lang_switch.html("EN");
		setLs("language", "hu");
	}
}

function changeLang() {
	let lang_switch = $(".lang");
	let lang = getLs("language");
	if (lang == "en") {
		$(".hu").show();
		$(".en").hide();
		lang_switch.html("EN");
		setLs("language", "hu");
	} else {
		$(".en").show();
		$(".hu").hide();
		lang_switch.html("HU");
		setLs("language", "en");
	}
}

function darkMode() {
	let dm = getLs("darkmode");
	dm == "dark" ? setMode("light") : setMode("dark");
}

function setColors(element, bg, txt){
	$(element).css({
		"background-color" : bg,
		"color" : txt
	});
}

// function setMode(p){
// 	let b = "#000";
// 	let w = "#fff";
// 	if (p == "dark") {
// 		setColors("*", b, w);
// 		setColors(".line", w);

// 		$(".logo img").css({
// 			"-webkit-filter" : "invert(100%)",
// 		   "filter" : "invert(100%)"
// 		});
// 		// $(".open-nav img").css({
// 		// 	"-webkit-filter" : "invert(100%)",
// 		//    "filter" : "invert(100%)"
// 		// });
// 		// $(".close-nav img").css({
// 		// 	"-webkit-filter" : "invert(100%)",
// 		//    "filter" : "invert(100%)"
// 		// });

// 		setLs("darkmode", "dark");
// 	} else if (p == "light") {
// 		setColors("*", w, b);
// 		setColors(".line", b);

// 		$(".logo img").css({
// 			"-webkit-filter" : "",
// 		   "filter" : ""
// 		});
// 		// $(".close-nav img").css({
// 		// 	"-webkit-filter" : "",
// 		//    "filter" : ""
// 		// });
// 		// $(".open-nav img").css({
// 		// 	"-webkit-filter" : "",
// 		//    "filter" : ""
// 		// });

// 		setLs("darkmode", "light");
// 	}
// }

function setLs(key, val) {
	localStorage.setItem(key, val);
}

function getLs(key) {
	return localStorage.getItem(key);
}